<div align="right"><h5>18.12.2019</h5></div>
<hr>
<div align="center"><h1 align="center">Large Systems Development Report</h1></div>
<div align="right"><h3 align="right"><em>by Group Three</em></h3></div>
</br>

---
This report presents the solution to the **Faraday car rental** business case. Briefly, the case was to build an application used by a car rental company to assist in the booking of cars. The business case in its entirety can be found at this [link](https://datsoftlyngby.github.io/soft2019fall/LSD/week-36/case_car_article.pdf).  
During the creation of this application the team was divided into two smaller teams. One team responsible for the frontend development and one team responsible for the backend development.
Both teams used their own repositories during development. This was done to ease the issue handling, specific to either the frontend or backend.  

The links to these can be found here:  
Frontend repo: https://gitlab.com/lsd-three/frontend  
Backend repo: https://gitlab.com/lsd-three/backend  
The contract containing the interface implemented by both of the teams can be found in this repository: https://gitlab.com/lsd-three/Contracts  
The product developed by the entire team can be found here: 
http://46.101.241.48:9090/frontend/

We have created a **user manual** for navigating around the system. It can be found [here](./UserManual.md).

<br/>

---
<a name="toc"></a>
## Table of Contents 
- [System Architecture](#architecture)
  - [Database](#db)
  - [Tests](#tests)
- [Tools](#tools)
  - [UML](#uml)
  - [Maven](#maven)
  - [Tomcat](#tomcat)
  - [Static Analysis](#staticanls)
  - [Selenium](#selenium)
  - [Gitlab](#gitlab)
  - [Docker](#docker)
  - [Digital Ocean](#do)
  - [Artifactory](#artifactory)
  - [Monitoring](#monitoring)
  - [Logging](#logging)
  - [Slack](#slack)
- [Risk metrics](#risks)
- [SLA](#sla)
- [Resouces](#resources)

<br/>

---
<a name="architecture"></a>
## System Architecture

<div align="center"><img src="./images/backend/architecture.png" align="center"></div>

Depicted in this diagram, we have our three droplets with various content. First droplet contains our main application, consisting of the frontend running in Tomcat, and a backend application running as an **Ubuntu** service by the **JVM**. Both the frontend and the backend communicate through RMI with implementation of the agreed upon the contract. This is by choice emphasised in our diagram. Also depicted is our droplet for the logging. It is running in a simple Node.js environment and receives logs from the backend which can be displayed for debugging purposes. Lastly, our monitoring droplet is illustrated. This droplet also contains a simple Node.js service that gathers monitoring information that is then used by Prometheus, and also fed into Grafana for prettier display of our monitoring data.

<br/>

<a name="db"></a>
### Database
Throughout this section, the different types of databases used by the backend will be presented. Having this separation helped the team to develop in an easy way with a database dedicated for development and testing, and provided a nice environment for production with another database dedicated for it.

##### Mysql
This database engine was used with the intention of persisting data in a stable way. It was the engine we have used for our production database as it meets all the teams’ needs and had been a preferred choice in several other projects, leading to some familiarity in the environment and providing ease of development when time was sparse.  
The fact that this engine is a relational type one was also a great contributing factor to the decision of deploying it, as it went in agreement with the previously discussed decisions of using a relational model for our data.

##### H2
Within the Java environment we needed a database that was quick to spin up, and supported a relational data model and that would either live in memory or in a small file within the project structure. For all of those reasons the H2 engine was chosen, it flawlessly connects through our JDBC driver and provides a fast but reliable development environment.  
One of the greatest advantages came when some integration tests had to be done, where the speed of this database shined through, allowing our integration tests to run in milliseconds instead of seconds at times. Additionally, each test could start with a fresh database state.

<br/>

<a name="tests"></a>
### Tests
##### Selenium
Selenium provides a playback tool for authoring end-to-end testing through the UI. The approach taken has been to, as close as possibly emulate, the user actions on the application. Included in the end-to-end test, is the possibility to run either Selenium as headless (not displaying the UI while the tests are executed), or with the “head”. Note that in order to execute test including the browser “head”, the browser binaries need to be located on a machine able to execute the mentioned binaries. The tests are executed in a pipeline - this means running Selenium headless. Note that the code includes a comment with an example of how the Selenium tests can be run with a “head”.

<br/>

---
<a name="tools"></a>
## Tools

<a name="uml"></a>
### UML
This standardized modeling language was chosen due to the advantages it provides when analysing and documenting the behaviour of the system. Furthermore, at the early stage of designing, it enabled us for an easier representation of our ideas to the lecturer and our peers. It was also easier to highlight some flaws which were taken into consideration when constructing the new look of the revised diagrams.  
Modelling software artefacts was very pleasant to do with the help of [draw.io](https://www.draw.io) since its interface was preferred to other tools that the team was interacting with before. On top of this, it provided more automation to the diagrams’ iteration flow. Its association with [Google Drive](https://www.google.com/drive/) was a key point for speedy loading and saving of artefacts. For keeping track of versioning we decided to make a [repository in Gitlab](https://gitlab.com/lsd-three/Artifacts), accommodating the exported UML diagrams.

A list of all the diagrams created, following UML standards, for analysing and documenting our system’s behaviours will be provided next:
##### **Logical data model** <br/>

After having brainstormed all the objects (nouns) with their potential corresponding data, the team created a number of entities under labels from the real world. While doing that we tried to logically place those in interrelationships with their associated attributes.
While modeling the data the cardinalities were analyzed and then represented as part of the logical data model, demonstrating the different behavior that can be associated between the mapped entities. The implementation strategy was to gain as much knowledge about the business case as possible at an early stage, avoiding confusing the domain concepts with others later on. This model also helped the team by giving preliminary names that could be used in properties and classes in the implementation.

##### **Use case model** <br/>

While discussing the Use case descriptions, the team spotted the disadvantage of not being able to provide a simplified way for each other to imagine the abstract operations in the system. Thereby the team began working on a Use case diagram. Its purpose was to give a visual representation of the Use cases which helped to depict the involved actors, and their involvement with the system. The diagram has enabled easy communication with our lecturer and peers, which contributed to discarding some possible flaws in the Use case descriptions. The team also used a notation that perfectly suited our case. This explanation is as follows:  
<div align="center"><em>The “extends” arrow is drawn from a Use case X to another Use case Y to indicate that the process of doing X always involves doing Y at least once as it adds additional actions on top of Use case Y.</em></div>

##### **Sub-system sequence diagram** <br/>

This diagram illustrates how certain tasks are performed between users and the system, regarding a single use case. This type of UML artifact also demonstrates what calls are  invoked between the frontend and backend parts of the system. By designing this diagram it was easier to represent in a high-level overview how the actions, activated by an actor, would impact the system.

##### **RDBM** <br/>

For designing a relational database model (RDBM), the team revised their logical data model and from there we extracted the key entities to be persisted in a database. All the relations that an entity is associated with were analyzed and after that, a relational database model was created. One of the strengths of this model, was to ensure that our design is free from anomalies, after normal forms were obeyed. Additionally, the entire team was on the same page about the records and their details stored in the database, which made it easier when the testing phase came into use. 

<br/>

<a name="maven"></a>
### Maven
Maven is a build automation tool primarily used for Java projects. This tool provides us with the ability to have a `pom.xml` file, that contains information about all the dependencies the project requires, and when Maven builds the project it ensures that these dependencies are met, by getting them from the local repository or fetching them online. Maven is also used to run unit testing when building the application, and creating reports of these. We use Maven builds for automatically getting our contract build file from the **jFrog** artifactory, as well when compiling the frontend and backend projects.
We used different builds by Maven for the frontend and backend application, as we wanted a `.war` file for running the frontend in Tomcat and we used a `.jar` file as our build to run the backend.

For the frontend, we used maven to run builds, and testing both in local development and in the pipeline. Its outcome was producing a `.war` file, which is a bundle of the application and its dependencies.

<div align="center"><img src="/images/backend/maven_1.png"></div>

We use maven to build a `.war` file - we supply a flag so that when we build we do not run all our tests.
Then we have a separate step for running maven but with tests that generate code coverage reports for Sonarcloud’s static analysis.

<div align="center"><img src="./images/backend/maven_2.png" align="center"></div>

On the backend we use Maven similarly but we produce a `.jar` file, as we found that a `.war` file running in tomcat did not work with RMI, as it did not allow for the system to create a Registry for the RMI, trough tomcat. This was a requirement for us, so when we used CI/CD we would ensure that the old Registry was removed and a new one was set up.

<br/>

<a name="tomcat"></a>
### Tomcat
Tomcat is an open-source Java Servlet engine, that provides support and runtime for Java web applications.  
This tool enables us to run our frontend on the server as a `.war` file, fairly easily. This also ensures that we can render JSP(Java Server Pages) that the frontend application is built with, to provide the HTML pages.


<br/>

<a name="staticanls"></a>
### Static Analysis
#### SonarCloud
We chose to use SonarCloud<sup id="r2"><a href="#res">**2**</a></sup><a name="r2"></a> as our static analysis tool, as it is very powerful and at the same time, very easy to manage and setup. We created a project both for the frontend and the backend. Due to that frontend and backend were already two separate projects, we wanted to be able to analyze them individually. When you create a project in SonarCloud, a project key, a token and some other configurations are being generated for you. You use these configs when you want to execute an analysis, like this:  

`mvn sonar:sonar -Dsonar.projectKey=<your-given-project-key -Dsonar.organization=team-three-lsd -Dsonar.host.url=https://sonarcloud.io -Dsonar.login=<your-given-token -Dsonar.jacoco.reportPaths=./target/jacoco.exec`  
This command is easily executable from our CI/CD pipeline. 
<br/>

#### Jacoco
As you can see, the last parameter to the command above is regarding Jacoco, which we use in our pipeline to generate a code coverage report reporting how much of our code we have covered with tests. This code coverage report is being fed into SonarCloud, which processes it and displays the code coverage percentage. 

It is possible to do a lot of things with SonarCloud, but we kept it really simple, as we had other priorities to focus on. But a thing we could have done was for example setting a minimum boundary for the code coverage percentage, and if it is below the minimum, we could fail the pipeline for not fulfilling the requested terms. However, the integration of the code coverage report and SonarCloud analysis is only present for the backend pipeline.  
Jacoco is integrated in our pipeline when we run the `mvn test` command. We include Jacoco by placing it as a plugin in the `pom.xml` file. The configuration looks as follows: 

<div align="center"><img src="./images/backend/jacoco.png" align="center"></div>

We specify the plugin with a <plugin> section containing a <phase> tag specifying which phase in the pipeline it should we executed within. Nothing else has to be done here. Maven takes care of the rest by taking use of this plugin. Another thing that is important to mention in this setup is that `mvn test` has to run before the execution of the SonarCloud analysis, because SonarCloud is dependent on the generated Jacoco report from the same `mvn test` command.

<br/>

<a name="selenium"></a>
### Selenium
_Please refer to <a href="#Tests">tests</a> section to find iformation on how we used this tool._

<br/>

<a name="gitlab"></a>
### Gitlab
##### About Gitlab
Gitlab is used for hosting our repositories. Gitlab is a tool that combines all the different online tools one would use in the development of a system. It is essentially an online DevOps platform, that provides different tools including **CI/CD**, **repository management**, **issue tracking**, **maven** & **docker repositories** and more, but some of the advanced things are pay only.
<br/>

##### Issue tracker
The issue tracker, found in the left menu, was used for organizing the project planning. We did strive to put on labels on every item to make use of the built-in Kanban board, found under _Issues -> Boards_. 

> **Backend experiences**  <br/>
In the backend team we used Gitlabs Issue system as a sort of a kanban board. We created issues for all parts of the development we planned in the beginning, so team members could assign themselves to the issue and do the necessary work for completing a task.

<div align="center"><img src="./images/frontend/issues.png" align="center"></div>
<br/>

We had several swimlanes for status of the task - they all started in the **Backlog** column, then went to **Doing**, and when finished they went to **Review** and a pull request was created for the issue. After that, another team member had to review the pull request, and merge it to the `master` branch if it passed all the tests and the code seemed reasonable.  
This was the way we tried to work in an agile way with the project, using Gitlab as our platform for handling our tasks.  
After using it for a short while, we were not impressed with this platform, and regretted not using something as Trello or Jira. The issues we had was the missing ability to add subtasks to a task, and the not responsiveness in general.
<br/>

##### CI/CD
The CI/CD chain is stored in `.gitlab-ci.yml` in the root of both the frontend and backend repo. Every time a commit has been pushed to Gitlab, this tool will run the pipeline on one of the shared workers.  
The pipeline uses some environment variables which are defined in the repository _settings option (left menu in the bottom) -> CI/CD (same menu) -> Variables_. Those variables are automatically injected when the pipeline runs and can be accessed as ordinary Linux environment variables. Using environment variables keeps the login credentials in secret.  
We used Gitlab’s caching capabilities to cache our dependencies, so when we run `maven` commands, we did not have to download all the dependencies every time the pipeline was executed, if they were still the same, this would speed up our pipeline for consecutive usages.

The login information to the Artifactory is defined in the variables: `MAVEN_REPO_PASS`, `MAVEN_REPO_URL`, and `MAVEN_REPO_USER`.
And the ssh credentials for the deployment is defined in `SSHPASS` and `SSHUSER`.
Also found in the left menu is the rocket CI/CD logo. Clicking this will navigate the user to a page where all executed pipelines can be found. 

We are using **Sonar Cloud** on both the frontend and backend, this integration is done here in the Gitlab CI/CD pipeline in the **test** stage, this is used for creating reports for code coverage, static analysis and unit tests.

> **Frontend experiences**  <br/>
In the pipeline, it’s worth mentioning that we have the following steps: <br/><br/>
**Build**  
This builds the `.war` file and stores it for the next step, in Gitlab, each step is run in separate containers, hence every file you need in the following steps needs to be specified as artifacts. <br/><br/>
**Deploy**  
Moves the `.war` file over to the Digital Ocean’s droplet with `ssh`, the server is using the new JSP files automatically. <br/><br/>
**Integration/end-to-end testing** 
The end-to-end test for the frontend is then being run using Selenium, hosted on an external droplet, for testing the newly deployed version.
<br/>

> **Backend experiences**  <br/>
In the backend pipeline every branch was tested, so that we knew that all the tests still passed before merging it into the `master` branch which also deployed the code to our server.  
The pipeline we created consisted of three stages in named order, namely : _test_, _build_, _deploy_.  
All our branches for the backend would run through the first testing stage. This stage ensures that if unit tests fail we would get a notice by email from Gitlab, and the pipeline will fail. We used this to ensure that pull requests, would not be merged if they did not pass the pipeline.  
When a commit was pushed to the `master` branch, it would also run the two other stages, starting with build. 
<br/>

<div align="center"><img src="./images/frontend/backend_experiences_1.png" align="center" width=80%></div>
<br/>

This stage would create the build file for deployment, and before building it we would ensure that we use the `persistence.xml` file that provides credentials for the ORM to the production database.  
Then run a `maven build`, but skip everything test related as we already had run the testing stage and to reach this stage the previous stage should have passed. We saved the build file as an artifact on Gitlab, and proceeded to the next stage where this artifact would be deployed to our server.

<div align="center"><img src="./images/frontend/backend_experiences_2.png" align="center"></div>
<br/>

The deploy stage was by far the most complicated step we had. We fetched the `sshpass` tool, this is a command line tool that enabled us to do `scp` command and providing the password in the same line, as this was a difficult task in a single line without the tool.  
We _secure copied_ the backend artifact file to the server, then we logged on to the served and used a linux service we had created to run the `.jar` file, and restart it if it stopped by error. This service gave us the ability to stop, start and check the status of the service.  
Then we set the thread to sleep for 5s allowing the service to start up. Then we used a `curl` command from Gitlab to `ping` the endpoint on the backend to check if it was running. If something went wrong and it did not run, the stage would fail and the pipeline fails. This would trigger an email to the one that did the commit, and it could quickly be handled. Optimally, we should have triggered the previous pipeline that had passed all stages, but we did not have the time to implement this feature.
<br/>

##### Wiki documentation
In gitlab, we use a dedicated Wiki area to keep our important related documentations directly in our repository. The goal is to keep the specific documentation for the project where the code resides.  
Our general strategy for maintaining the Wiki pages was that whoever worked on a relevant entry to the Wikis, had the responsibility to update the Wikis.  
The Wiki entries were created and updated continuously during the project development, with the aim to act as direct handover documentation, to make it possible for other developers to maintain our code as easily as possible.
<br/>

**What we would have liked to do**  
We would have liked to use the built-in container repository, but at the moment, we deploy the generated `.war` files to the server directly. This is something we would like to change.  
If we had access to a paid version of Gitlab, we would have liked to use Gitlabs maven repository instead of the Jfrog Artifactory, and see what the code analyzer could have done compared to SonarCube.
<br/>

**More reflection on experiences with Gitlab**  
The biggest problem with Gitlab was the stability, at times, it could be very slow or not available at all. The pipeline runners could also be a problem when no shared worker was available. Both of those problems could have been solved by hosting Gitlab ourselves because we don’t have to share the service with others.

The issue tracker was only used in the backend group, where it was both used to track issues from the frontend group and internal progress.  
The main flow on the communication of issue discover between the frontend and backend was done through both the built-in issue tracker in Gitlab (the contract repository) and also the external tool Slack, in which we created a _general_ channel for the entire team and a specific frontend channel to handle issue pertaining the frontend only.

<br/>

---
<a name="docker"></a>
### Docker
In this project we have been using Docker to help us develop the frontend. Docker is a tool used to containerize applications. Containerizing the application makes it easier to develop across platforms. Using Docker containers removes a lot of issues with dependencies from the users machine as docker will have all the ones we need. This fixes the common issue _"This works on my machine”_.  
By packaging everything up in a container, we can easily deploy the project to a server and have it up and running very quickly.  
This is the next step in development - one step further than virtual machines.

We have used Docker to make it easier for us to both deploy and work on the project from different machines. In our development group we have Linux, Windows and macOS, and to be sure that they all work in the same way, we can use Docker to make sure of that.

In our configuration for Docker, “**[Dockerfile](./Dockerfile)**” we have put many commands that Docker will follow, in order to run our project.  

_Line 1_: We start by telling Docker to use the Maven image file from Docker’s own repository. We also specify the version we need in the same command.

_Line 3-5_: In order for Maven to have access to one of our artifacts, we need to specify some arguments. Such secrets should not be put on Gitlab or any other public place, therefore, we will use them as command line arguments.

_Line 6_: Next, we specify our working directly, this is relative to the path that the Dockerfile is.

_Line 8-9_: We continuing with copying our `pom.xml` and `.m2` folder directly into the docker environment.

_Line 11-14_: After that we run the Maven’s command line inside the Docker with our own configuration files. `-f` flag is for our own `pom.xml` and `-s` for our own settings.

_Line 16_: Simply copies all of our code into the container.

_Line 18_: Runs the `maven package` command that compiles the code and packages it in a distributable format such as `.jar` or `.war`.

_Line 20_: We need Tomcat for those steps, so we take the image from Docker.

_Line 22_: We remove the old files from earlier if they are there.

_Line 24_: We put the new build from Maven into the root folder of Tomcat, so it will use that project.

_Line 27_: Exposes port 8080 so Docker will listen to the specified port.

_Line 29_: Simply runs the entire thing.

<br/>

---
<a name="do"></a>
### Digital Ocean (droplets and hosting)
A droplet is a virtual private server (VPS) using a Linux based virtual machine (VM), that can be created on a shared hosting environment. Our droplet is created in Frankfurt. Tomcat is directly installed on the backend.

The application is running on 3 droplets.

1. The application contract is hosted on a droplet at IP : [167.172.108.3:8081](http://167.172.108.3:8081) _(for login credentials check the handed in document)_. The Artifactory is running as a docker container - `docker.bintray.io/jfrog/artifactory-oss:latest`.

2. We have another droplet running the application (both frontend and backend). The backend consists of `.jar` file running in an **Ubuntu** service and containing Mysql installation. The frontend is served by the same Tomcat server. The  application is running on the following URLs: 
- Frontend: [http://46.101.241.48:9090/frontend/](http://46.101.241.48:9090/frontend/)   
- Backend: [http://46.101.241.48:9090/backend/](http://46.101.241.48:9090/backend/)  

Should the administrator be interested in running the frontend inside of a docker container, instead of the installed Tomcat server, on the droplet, the administrator can execute the following command.

**Build command**:  
`docker build --build-arg MAVEN_REPO_USER=admin --build-arg MAVEN_REPO_PASS="`**see handin file for login credentials**`" --build-arg MAVEN_REPO_URL="http://167.172.108.3:8081/artifactory" --build-arg ENVIRONMENT=PROD -t jsp-frontend`

**Run command**:  
`docker run -p 8080:8080 jsp-frontend`

3. A droplet running Selenium on a stand-alone server (Firefox) inside a docker container. The container running on this droplet is only used when there is a new build of the frontend. The URL for Selenium is [http://167.71.63.78:4444/wd/hub](http://167.71.63.78:4444/wd/hub).

4. A droplet running logging (Grafana, Prometheus) on another server. For further details check the information under the section <a href="#monitoring"><em>"Monitoring"</em></a>.  
[Prometheus](http://mathiasbigler.com:9090) _(no login is necessary)_  
_[Grafana](http://mathiasbigler.com:3002) (login provided in handin docs)_

5. We also have an external logging server which the logs are sent to: [awha.dk:9001](http://awha.dk:9001)

<br/>

<a name="artifactory"></a>
### Artifactory
In our project we stood with a problem that both the frontend and the backend team had to use some sort of shareable module of different ‘**Data transfer objects**’(DTOs), exceptions(ETOs) and interfaces. We had to somehow reuse our classes without reimplementing the exact same code both in the frontend and backend project. If we did not solve this issue, we had to both make changes to the frontend and the backend, which easily could turn into a problem in the form of double the effort and version control.  
To solve this issue, we made use of JFrog, which is an artifactory repository manager that makes you able to store binary modules of all sorts of programming languages in the Cloud. The reason we chose JFrog specifically is, because it supports Maven out of the box, and Maven is the bundle manager we use for our projects.

<div align="center"><img src="./images/frontend/jfrog.png" align="center" width=70%></div>
<br/>

We have our Contract project and our frontend and backend projects. The Contract project is used to create our shared module, and when we are done with our changes in the code, we push these onto our JFrog artifactory where it lies with a versioning number.  
Our frontend and backend then pull the contract binaries from the JFrog repository, which makes us able to share the exact same version and classes from the Contract module. We specify what binary and version we would like to get within our `pom.xml` file in both our backend and frontend project like this:

<div align="center"><img src="./images/frontend/pom.png" align="center" width=60%></div>
<br/>

In our latest example, we get the binaries from the path of `com.mycompany` and specify the artifact of `Faraday-Three-Car-Rental` which is our `.jar file` on JFrog. At last, we specify the version of the `jar` we would like to get, which makes it easy to do versioning for both the frontend and the backend group without stepping over each others feet. 

<br/>

<a name="monitoring"></a>
### Monitoring
The monitoring section of our system consists of three subsystems - a metrics collector, Prometheus and Grafana. These three subsystems are all interlinked and it makes sense to explain the metrics collector firstly. 
#### Metrics Collector
The metrics collector runs as a Node.js application on one of our servers. Its purpose is to generate metrics about the other subsystems in our system, which in our case are metrics regarding health checks and response times. The application serving metrics runs on following IP address: http://138.68.106.203:8282/metrics.  
The endpoint provides three different measurement criterias: a health check for the frontend, a health check for the backend and a response time check for the database. Our checks are rather simple, and we could have added more, but we had other priorities in the end. These checks are being executed and regenerated every five seconds.  
We expose these metrics using a **NPM package** called _"Gauge<sup id="r1"><a href="#res">**1**</a></sup><a name="r1"></a>"_ which is very easy to use. It works in the way that you register a metric like this: 

<div align="center"><img src="./images/backend/metrics_1.png" align="center"></div>

And then every five seconds, we execute this function:

<div align="center"><img src="./images/backend/metrics_2.png" align="center" width="60%"></div>

In our backend we set up a public endpoint that is available from the internet. We use it to ping the backend checking whether it is alive or not. If it is, we set the metric to `1` meaning that the backend service is alive. If it isn’t, we set it to `0`. 
These metrics are being served on an endpoint in a special format which Prometheus understands. To actually monitor and use these metrics, we need a monitoring system - this is where Prometheus comes into use. 
<br/>

#### Prometheus
Prometheus is a monitoring tool and is able to target certain metrics exposed by so-called metric _collectors_ or _exporters_. Our instance of Prometheus runs on following endpoint: http://138.68.106.203:9090/graph.  
To link Prometheus to the exporter you can register the metrics endpoint (shown previously) as a target in Prometheus, and from here, Prometheus does the rest of the work. You can specify how often Prometheus should scrape the metrics endpoint, and after this is done, Prometheus will do so at an interval of your choice. From here onward, Prometheus generates graphs displaying the history of the retrieved metrics data. But the downside is that this tool’s UI is not very descriptive, and this is where Grafana steps in. This is how Prometheus’ graphs looks like: 

<div align="center"><img src="./images/backend/prometheus.png" align="center"></div>
<br/>

#### Grafana
Grafana is used for visualization the metrics analytics and data. Our instance of Grafana can be seen on following endpoint: http://138.68.106.203:3002/?orgId=1.  
Grafana is able to register data sources, such as Prometheus. All the metrics data we have from the metrics collector are stored in Prometheus, so Grafana hooks itself up to Prometheus and is thereby able to display the metrics that Prometheus has in a much more organized and visual way. This is how our Grafana dashboard looks like with the three metrics: 

<div align="center"><img src="./images/backend/grafana.png" align="center"></div>

<br/>

<a name="logging"></a>
### Logging
The logging system of our backend system, consists of a few different parts: 
- Client logic to generate log output in the form of SocketAppender configuration
- A Log4J 2 Socket Server<sup id="r4"><a href="#res">**4**</a></sup><a name="r4"></a>
- Frontail, a streaming logs application<sup id="r5"><a href="#res">**5**</a></sup><a name="r5"></a>

#### SocketAppender
In our Backend project, the logic to generate log output in the form of SocketAppender configuration, is specified in the `log4j2.xml` file - located in the resources part of our project structure:

<div align="center"><img src="./images/backend/log4j_1.png" align="center"></div>

This file contains the basic configuration of the logging logic for our application:

<div align="center"><img src="./images/backend/log4j_2.png" align="center"></div>

A socket appender is specified to point towards the host _"awha.dk"_ at port _"8765"_. This is where the remote Log4J 2 Socket Server is hosted. 
Furthermore, the loggers _"Root"/Default_ log-level is set to "**info**".

The main areas in our Backend project that we decided to generate log messages from, is inside our Facade classes, namely to log general method behavior (log level "**info**") and exceptions (log level "**warning**"):

<div align="center"><img src="./images/backend/log4j_3.png" align="center"><em>BookingDBFacade.java<sup id="r6"><a href="#res"><b>6</b></a></sup><a name="r6"></a></em></div>
<br/>

By doing so, logs are generated to show general application behavior as well as exception scenarios.

#### Log4J 2 Socket Server
The Log4J 2 Socket Server is an implementation of Apache Log4j 2<sup id="r7"><a href="#res">**7**</a></sup><a name="r7"></a>, an open source logging framework for Java. The server is deployed as a Java `.jar` file, and configured to listen to incoming traffic on port _"8765"_. When started, it logs the log messages to a local `.log` file. 
It is executed from a shell script:

<div align="center"><img src="./images/backend/log4j_4.png" align="center"></div>

#### Frontail
Since the log files are appended to a remote server by design (to make log analysis possible, in case of production server outage), the remote logging server implements a simple streaming logs application called Frontail, implemented in Node.js. 
This simple application<sup id="r8"><a href="#res">**8**</a></sup><a name="r8"></a> tails the log files generated by the Log4J 2 Socket Server in _"follow"_ mode, thus visualizing the log file in the browser:

<div align="center"><img src="./images/backend/frontail.png" align="center"><br/><em>Data filter<sup id="r9"><a href="#res"><b>9</b></a></sup><a name="r9"></a></div>

<br/>

<a name="slack"></a>
### Slack
Slack<sup id="r3"><a href="#res">**3**</a></sup><a name="r3"></a> was the main communication tool both internally within frontend and backend, and between teams. It provided the convenience of sending direct messages between two or more people, setting up different channels with particular purposes which helped to separate from one another responsibilities.  
Additionally, Slack offers direct calls, screen share and other useful features. We created a channel for the backend, another for the frontend, as well as three more for deployment updates & alerts on our three development projects (Backend, Frontend, Contract).  
Slack is incredible regarding integration of API’s, which made it possible for us to integrate it with Gitlab. On every deployment Gitlab sent a message to a specific Slack channel with information about how the particular deployment went. This way, as you deploy, you don’t have to access Gitlab to see whether everything went as hoped for. If something goes wrong, you can find the details in Gitlab. We found this tool very useful and easy to communicate over as when new messages occur, it notifies the receivers, which makes responses faster.  
We also used slack with the purpose of keeping/sharing the group secrets and passwords of the different platforms we used, which shall not be public.

<div align="center"><img src="./images/backend/slack.png" align="center"></div>


<br/>

---
<a name="sla"></a>
## SLA
Our Service Level Agreement (SLA) is comprised of 7 overall criteria. These are the criteria that we, as a provider of the software, guarantee the software will uphold.
_The entire part of the SLA pertaining to the service desk/helpline has been omitted, since this application will not be backed up by a live operations-crew._
<br/>

##### Uptime/availability
Based on previous measurements, we concluded that we can attain > 95% uptime. View live measurements [here](http://138.68.106.203:3002/dashboard/db/faraday-carrental-backend-and-frontend?refresh=5s&orgId=1).  
_(Credentials are found [here](https://gitlab.com/lsd-three/backend/wikis/Credentials-and-other-info))_
<br/>

##### Mean response time
Based on basic HTTP GET request using `curl` from a client in Denmark to our hosted project in Frankfurt, we guarantee a mean response time < 500ms.
<br/>

##### Meantime to recover
The droplet uptime is not in the scope of this SLA to estimate, but we believe Digital Ocean will sustain high uptime.
Regarding recovery of the applications(s), they should be up within 6 seconds upon application crash. The reboot service is set to restart in 5 seconds interval.
<br/>

##### Failure frequency
We aim to keep the failure frequency low, as we don’t have sufficient data to get a more precise failure frequency estimation. _(With a longer testing period we would be able to provide specifics)_.
<br/>

##### Monitoring
We will provide monitoring for the application health status (link [here](http://138.68.106.203:3002/dashboard/db/faraday-carrental-backend-and-frontend?refresh=5s&orgId=1)), as well as [logging](http://awha.dk:9001) and [health check](http://138.68.106.203:8282/metrics) signals.
<br/>

##### Service
With this service we provide an application that follows the specified demands set in this document.
https://datsoftlyngby.github.io/soft2019fall/LSD/week-36/case_car_article.pdf (paragraph 1.3; The service)
<br/>

##### Issue Reporting
Should any issues arise, we have issue reporting functionality at this url: [https://gitlab.com/lsd-three/Contracts/issues](https://gitlab.com/lsd-three/Contracts/issues)
_Please try to be as specific as possible, when reporting, as this will help us in tracking down the issue faster._
<br/>

##### Repercussions
_This particular section is typically part of an SLA. Since this handover of software is limited to purely a school-project, the **repercussions** section is irrelevant. In a real-life handover of a software product, this section will include compensation, typically financial, for time/orders/goods lost._

<br/>

---
<a name="risks"></a>
## Risk metrics
Using the **OWASP Risk Rating Methodology** template, we estimate our hosted applications security risk of being exploited by the number one security risk, _Injection_. This is specified in the _“OWASP TOP 10 Most Critical Web Application Security Risks report for 2017”_.

<div align="center"><img src="/images/backend/risk_1.png" align="center"></div>

This gives us an overall Likelihood score of _5.5_, and an overall Impact score of _7.5_.  
Using The Security Matrix, this places our _Overall Risk Severity_ at **High**:

<div align="center"><img src="/images/backend/risk_2.png" align="center" width="70%"></div>

<br/>

---
<a name="resources"></a>
## Resouces
Additional resources:  
[Logical Data Model](https://gitlab.com/lsd-three/Artifacts/blob/master/Logical%20Data%20Model.pdf)  
[Use Case Model](https://github.com/Team-Three-LSD/Artifacts/blob/master/Use%20case%20diagram/Use%20case%20diagram%20v3.png)  
[Use Case Diagrams](https://gitlab.com/lsd-three/Artifacts/blob/master/Use%20case%20descriptions/Use%20case%20descriptions%20v3.pdf)  
[System Operation Contracts](https://gitlab.com/lsd-three/Contracts)  
[Branching Strategy](https://gitlab.com/lsd-three/Artifacts/blob/master/Branching_Strategies.pdf)

<br/>

Relevant Wikis:
#### Frontend
Handover docs: https://gitlab.com/lsd-three/frontend/-/wikis/Handover-docs-%F0%9F%93%96  
#### Backend  
How to install the backend: https://gitlab.com/lsd-three/backend/-/wikis/How-to-install-the-backend  
Local development: https://gitlab.com/lsd-three/backend/-/wikis/Local-development  
Logging: https://gitlab.com/lsd-three/backend/-/wikis/Logging  
Metrics: https://gitlab.com/lsd-three/backend/-/wikis/Metrics  
Pipeline Docs: https://gitlab.com/lsd-three/backend/-/wikis/Pipeline-Docs

<br/>

<a name="res"></a>
External resources:  
<b id="i1">1</b> https://www.npmjs.com/package/gauge [↩](#r1)  
<b id="i2">2</b> https://sonarcloud.io/about [↩](#r2)  
<b id="i3">3</b> https://slack.com/intl/en-dk/help/articles/115004071768-What-is-Slack- [↩](#r3)  
<b id="i4">4</b> https://github.com/piruin/log4j2-socket-server [↩](#r4)  
<b id="i5">5</b> https://github.com/mthenw/frontail [↩](#r5)  
<b id="i6">6</b> https://gitlab.com/lsd-three/backend/blob/master/src/main/java/Database/BookingDBFacade.java [↩](#r6)  
<b id="i7">7</b> http://logging.apache.org/log4j/2.x/ [↩](#r7)  
<b id="i8">8</b> https://en.wikipedia.org/wiki/Tail_(Unix) [↩](#r8)  
<b id="i9">9</b> http://awha.dk:9001/?filter=Data [↩](#r9)

<br/>

---
### Written by:

#### Frontend
`Benjamin Larsen` &nbsp;&nbsp; :e-mail: : [cph-bl135@cphbusiness.dk](mailto:cph-bl135@cphbusiness.dk) <br/>
`Jacob Sørensen` &nbsp;&nbsp;&nbsp;&nbsp; :e-mail: : [cph-js284@cphbusiness.dk](mailto:cph-js284@cphbusiness.dk) <br/>
`Mathias Igel` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :e-mail: : [cph-mi73@cphbusiness.dk](mailto:cph-mi73@cphbusiness.dk) <br/>
`Oliver Lønning` &nbsp;&nbsp;&nbsp;&nbsp; :e-mail: : [cph-ol31@cphbusiness.dk](mailto:cph-ol31@cphbusiness.dk) <br/>
`Yosuke Ueda` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; :e-mail: : [cph-yu173@cphbusiness.dk](mailto:cph-yu173@cphbusiness.dk) <br/>

#### Backend
`Alexander Andersen`  &nbsp;&nbsp;:e-mail: : [cph-ah353@cphbusiness.dk](mailto:cph-ah353@cphbusiness.dk) <br/>
`David Alves` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:e-mail: : [cph-dd69@cphbusiness.dk](mailto:cph-dd69@cphbusiness.dk) <br/>
`Elitsa Marinovska`  &nbsp;&nbsp;&nbsp;&nbsp;:e-mail: : [cph-em95@cphbusiness.dk](mailto:cph-em95@cphbusiness.dk) <br/>
`Mathias Bigler` &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;:e-mail: : [cph-mb493@cphbusiness.dk](mailto:cph-mb493@cphbusiness.dk) <br/>
`Stanislav Novitski` &nbsp;&nbsp;:e-mail: : [cph-sn183@cphbusiness.dk](mailto:cph-sn183@cphbusiness.dk) <br/>