# Faraday Car Rental - user manual
The Faraday Car Rental application enables the user to perform a car booking online.  
From the main/landing [page](http://46.101.241.48:9090/frontend/) the user will find options to:
- Add a new booking 	
- Search for bookings by driver id 	
- Search for a booking by booking id 	

<br/>

The _**Add new booking**_ feature takes the user to a menu where the user enters the relevant booking details, including the _type of car_ the user wishes to rent, _pickup location_, _rental period_ and so on.  
During the booking process, the user will get a chance to see relevant info for each car available for booking, such as: the _rental price_ per day, the _type of car_, _number of seats_ in the car.  
When the user completes the booking process the system will either:
- _in case of success_: respond with the rental price and your booking id (a unique identifier for each booking);
- _in case of failure_: respond with an appropriate error message;

<br/>

The _**Search by driver id**_ feature will take the user to a menu where the user can enter a specific driver id.  
The system will either:
- _in case of success(valid id)_: respond with a list of total booking made by the particular driver id _(note: this list can contain just a single booking)_;
- _in case of failure: respond with an appropriate error message;

From the list provided by the system, the user has the option to _**cancel a specific booking**_.

<br/>

The **Search by booking id** feature will take the user to a menu where the user enters a specific booking id.  
The system will either:
- _in case of success(valid id)_: respond with a single booking matching the id provided by the user;
- _in case of failure_: respond with an appropriate error message;

As opposed to the option mentioned previously, this will always return a maximum of one booking since the booking ids are unique. If/When the system returns a booking the user has the option to cancel this booking.

<br/>

_Additional_  
The user can at any time click the _"Faraday"_ logo in the menu pane - this will direct the user to the front page. 
If a booking is canceled the user is directed to a page confirming the cancellation.
